# extended_burden_algorithm

Pipeline to perform burden test for increased mutational burden in different case/control cohorts based on genotyped data.\n
Based on and extension of:


### 1. MONSTER:  

SNPset association testing using mixed effect models to allow inclusion of related or admixed samples\n

<a href="https://www.stat.uchicago.edu/~mcpeek/software/MONSTER/MONSTER_v1.3_doc.pdf">MONSTER Documentation</a>



### 2. mummy: 

extension of gene regions with associated regulatory regions\n

<a href="https://github.com/hmgu-itg/burden_testing">Mummy GitHub</a>
