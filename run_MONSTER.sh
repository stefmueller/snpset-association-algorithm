#!/bin/bash

# start pheno file: plink fam file
PHENO="/home/smueller/data/bcac/scripts/A_make_monster_input/A03_pheno/MONSTER.pheno.txt"

# start genotype file: plink bed file
GENO="/home/smueller/data/bcac/scripts/A_make_monster_input/A02_impute_to_monster/MONSTER.geno.txt.gz"

# start snp file: plink bim file
SNPS="/home/smueller/data/bcac/scripts/A_make_monster_input/A04_snplist/MONSTER.snplist.txt"

KIN="/home/smueller/data/bcac/scripts/A_make_monster_input/A01_rel_mat/test.txt"

zcat $GENO >"MONSTER.geno.txt"

MONSTER \
    -p $PHENO \
    -g "MONSTER.geno.txt" \
    -s $SNPS \
    -k $KIN \
    -m 0 # genotype missingess threshold set to 0 to prevent imputation of missing genotypes

rm MONSTER.geno.txt

#TODO
# find a way to feed in compressed genofile (xargs maybe?)
