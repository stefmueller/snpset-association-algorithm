#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/03/26

# --- OBJECTIVE: create pheno file for MONSTER input

# --- REQUIREMENTS:
# none

# --- INPUT:
# FAM: plink fam file of genotyped dataset
# SAMPLE: Sample file from A02 after removal of samples

FAM="/home/smueller/data/bcac/scripts/A_make_monster_input/data/onco_african_LD_pruned.fam"
SAMPLE="temp.oxford_clean.sample"

tail -n +3 $SAMPLE | cut -d " " -f1 >"temp_samples.txt"
grep -F -f "temp_samples.txt" $FAM >temp_MONSTER.pheno.txt
sed 's/^[0-9]*/111/g' temp_MONSTER.pheno.txt  >MONSTER.pheno.txt


#rm temp* 
