#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/04/04

# --- OBJECTIVE: flag SNPs with insufficient imputation certainty or aligned to major allele

# --- REQUIREMENTS:
# none

# --- INPUT:
# INFO: impute2 info INFO
# infoThresh: threshold for impute2 info score metric
# BED: bed file with regions of interest 

#INFO="/home/smueller/data/bcac/files/01_imputed_genos/onco_imp_african/chr22/OncoArray_chr22_African_phased_51050415_51243304.txt_info"
INFO=$1
infoThresh=$2
BED=$3

# --- STEP1: flag bad info scores
if [ -f snp_flag_info_bad.txt ];then rm snp_flag_info_bad.txt ;fi 

while read CHR START STOP NAME
do

    awk -v thresh=$infoThresh ' $7 < thresh  {print $2, $3 }' $INFO |\
    awk -v start=$START -v stop=$STOP '$2 >= start && $2 <= stop {print $1 }' \
    >>snp_flag_info_bad.txt

done<$BED

# --- STEP2: flag snps to flip minor + major definition;
# at same time ensure SNPs fulfil info filter to avoid unnecessary flipping
if [ -f snp_flag_flip_alleles.txt ];then rm snp_flag_flip_alleles.txt ;fi
while read CHR START STOP NAME
do
    awk -v thresh=$infoThresh ' $6 > 0.5 && $7 > thresh { print $2, $3 }' $INFO |\
    awk -v start=$START -v stop=$STOP '$2 >= start && $2 <= stop {print $1 }' \
        >>snp_flag_flip_alleles.txt
done<$BED
#TODO
# include maf threshold
