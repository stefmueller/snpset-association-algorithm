#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/04/04

# --- OBJECTIVE: generate minor allele dosage file as Input for MONSTER

# --- REQUIREMENTS:
# none

# --- INPUT:

# CHR: chromosome
# GEN: impute2 dosage genotypes
# SAMPLE: Oxford sample information file, see also "https://www.cog-genomics.org/plink/2.0/formats"
# SNPinfoBAD: text file with SNP IDs of markers with insufficient info score
# SNPmajor: text file with SNP IDs of markers aligned to major and not minor allele

CHR=$1
GEN=$2
SAMPLE=$3
SNPinfoBAD=$4
SNPmajor=$5

### retrieve sample number
Nsam=$(tail -n +3 $SAMPLE | wc -l | cut -d " " -f1)
echo "Number of samples is $Nsam"

# --- STEP1: make mean geno file for correct minor allele defined markers
# first remove markers flag for bad info score,
# then remove markers which should be flipped,
# then create new output file with first column containing new SNP identifier
# concatenated from CHR, POS, Major allele and Minor Allele and subsequent
# columns holding information of imputed minor allele counts, i.e. one column per sample


cat $GEN |\
grep -vf $SNPinfoBAD |\
grep -vf $SNPmajor |\
awk -v chr=$CHR -v s=$Nsam '{ printf chr "_" $3 "_" $4 "_" $5 ; for(i=1; i<=s; i++) printf "\t" $(i*3+5)*2+$(i*3+4); printf "\n" }' >geno_one.txt

# --- STEP2: make mean geno file for wrongly defined minor alleles
# first select markers flag for flipping of major and minor allele,
# then create new output file with first column containing new SNP identifier
# concatenated from CHR, POS, Major allele and Minor Allele (already flipped) 
# and subsequent columns holding information of imputed minor allele counts,
#  i.e. one column per sample
cat $GEN |\
grep -f $SNPmajor |\
awk -v chr=$CHR -v s=$Nsam '{ printf chr "_" $3 "_" $5 "_" $4 ; for(i=1; i<=s; i++) printf "\t" $(i*3+3)*2+$(i*3+4); printf "\n" }' >geno_two.txt

# --- STEP3: bring geno info together
cat geno_one.txt geno_two.txt | gzip >temp_geno.txt.gz
rm geno_one.txt
rm geno_two.txt

# --- STEP4: create header file
echo -n "0	" >temp_header.txt # -n option suppresses new line character
cut -d " " -f1 $SAMPLE | tail -n +3 | paste -d "\t" -s >>temp_header.txt
gzip temp_header.txt

# --- STEP5: bring everything together
cat temp_header.txt.gz temp_geno.txt.gz >MONSTER.geno.txt.gz

# --- STEP6: clean up
#rm temp*

#TODO:
# take compressed input (substitute cat with zcat)
