#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/04/04

# --- OBJECTIVE: remove samples from Impute2 data using plink2 and extract genomic region of interest
# Impute2 gen format is without sample header so use plink2 to securely identify
# samples to remove by name 

# --- REQUIREMENTS:
# plink2

# --- INPUT:
# CHR: chromosome 
# GEN: compressed impute2 dosage genotypes
# SAMPLE: Oxford sample information file, see also "https://www.cog-genomics.org/plink/2.0/formats"
# REMOVE: text file with sample ids to remove, one sample per line
# BED: bed file with genomic regions of interest

CHR=$1
GEN=$2
SAMPLE=$3
REMOVE=$4 
BED=$5
#REMOVE="samples_to_remove.remove" #list with 4 samples  

# --- STEP1: read dosage data and sample file (created on the fly in R based on
# sample order file)
# need --oxford-single-chr flag since impute2 files not correctly formatted
# need to create temporary pgen file otherwise error is thrown


plink2 --gen $GEN \
    --sample $SAMPLE \
    --remove $REMOVE \
    --oxford-single-chr $CHR \
    --extract $BED \
    --export oxford \
    --out "temp.oxford_clean"

#cp "temp.oxford_clean.sample" ../A03_pheno

#TODO:
# compress output
# rename markers in a uniform way
# re-order samples consistently
