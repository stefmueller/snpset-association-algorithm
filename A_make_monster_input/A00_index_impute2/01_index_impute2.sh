#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/04/03

# --- OBJECTIVE: crete index for genomic positions in imputed genotypes

# --- REQUIREMENTS:
# R, R-packages: tidyverse, optparse

# --- INPUT:
# DIR: path to directory holding impute2 genotypes
# RSCRIPT: path to Rscript for data formatting

DIR="/SAN/ugi/bcac/files/01_imputed_genos/onco_imp_african"
RSCRIPT="/home/smueller/data/bcac/scripts/A_make_monster_input/A00_index_impute2/make_index.R"

# --- STEP1: find all gunzipped files with imputed genotypes

find $DIR -name '*txt.gz' > temp_files_path.txt

# --- STEP2: create index with in R
Rscript --vanilla "$RSCRIPT" -f "temp_files_path.txt" 

# --- STEP3: clean up
#rm temp*

