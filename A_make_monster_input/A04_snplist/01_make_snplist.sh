#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/03/26

# --- OBJECTIVE: create Snplist

# --- REQUIREMENTS:
# none

# --- INPUT:
# MONSTERGeno: compressed MONSTER geno input
MONSTERGeno="/home/smueller/data/bcac/scripts/A_make_monster_input/A02_impute_to_monster/MONSTER.geno.txt.gz"

# --- STEP1: create file and save snplist name and set switch for inclusion
# SNP weights
echo -n "SNPlist01 0   " >MONSTER.snplist.txt

# --- STEP2: append snp ids in same line
zcat $MONSTERGeno |\
    cut -f1 |\
    tail -n +2 |\
    paste -s >>MONSTER.snplist.txt
    
#TODO:
# add weights
# use different input
