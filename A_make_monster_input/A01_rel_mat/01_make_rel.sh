#!/usr/bin/env bash

# AUTHOR: smueller
# LAST_EDIT: 2019/03/26

# --- OBJECTIVE: create pairwise kinship as well as inbreeding coefficients for Monster

# --- REQUIREMENTS:
# gemma, plink1.9, R, R-packages: tidyverse, data.table, optparse 

# --- GENO:
# RSCRIPT: path to Rscript for data formatting
# GENO: plink binary genotype files after LD pruning (command: --indep 50 5 2)
# OUT: name of output file

RSCRIPT="/home/smueller/data/bcac/scripts/A_make_monster_input/A01_rel_mat/make_monster_kin_coef.R"
GENO="/home/smueller/data/bcac/scripts/A_make_monster_input/data/onco_african_LD_pruned"
OUT="test"

# --- STEP1: calculate gemma relationship matrix
gemma -bfile $GENO -gk 1 -o $OUT

# --- STEP2: calculate plink fhat statistics
plink1.9 --bfile $GENO --ibc --out $OUT

# --- STEP3: merge gemma and plink output and bring in monster GENO format
Rscript --vanilla "$RSCRIPT" -g "output/${OUT}.cXX.txt" -p "${OUT}.ibc"

# --- STEP4: clean up
#rm test*
#rm -r output/


#TODO:
# replace plink1.9 with plink2 so only one plink version necessary in pipeline
# reduce R library dependencies
# speed up reformatting in R
